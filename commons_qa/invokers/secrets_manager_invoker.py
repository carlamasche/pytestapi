"""
Archivo py con la clase encargada de conectarse con el servicio Secrets Manager de AWS
"""
import commons_qa.logger as logger
import json
from commons_qa.configs.authenticator import Authenticator


class SecretsManagerInvoker:
    """
    Esta clase contiene los métodos necesarios para conectarse con el servicio Secrets Manager de AWS
    """
    AWS_SERVICE_NAME = 'secretsmanager'

    def __init__(self, account):
        self._client = Authenticator.for_account_and_service(account, self.AWS_SERVICE_NAME).client
        self.logger = logger.get_logger(__name__)

    def get_secret_value(self, secret_id):
        """
        Este método permite traer un valor asociado al secret_id pasado por parámetro

        :param secret_id: Id de la key del par clave - valor a traer
        :return type: json (dict)
        :return: Valor correspondiente al SecretId

        Ejemplo::

            secret_manager = SecretsManagerInvoker('bancar').get_secret_value('apiKeyAmplitude')
            amplitude_api_key = secret_manager['api_key']
            amplitude_secret_key = secret_manager.get('secret_key')
        """
        self.logger.info(f"Retrieving secret value for SecretId: '{secret_id}'")
        return json.loads(self._client.get_secret_value(SecretId=secret_id)['SecretString'])

