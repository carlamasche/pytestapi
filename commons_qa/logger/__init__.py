"""
Paquete que contiene el manejo de logs
"""

import logging
import sys


def get_logger(name=None, log_level=logging.DEBUG):
    """
    Método que disponibiliza un objeto logger que permite loguear los mensajes necesarios en diferentes niveles (INFO, DEBUG, ERROR, etc)
    """
    logging.basicConfig(stream=sys.stdout,
                        format='%(asctime)s - %(levelname)s - %(filename)s - %(lineno)d - %(message)s',
                        level=log_level,
                        datefmt='%Y-%m-%d %H:%M:%S')
    logger = logging.getLogger(name)
    return logger
