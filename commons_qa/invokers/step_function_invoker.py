"""
Archivo py con la clase encargada de conectarse con el servicio Step Function de AWS
"""
import datetime

import polling2

from commons_qa import logger
from commons_qa.configs.authenticator import Authenticator
from commons_qa.profiles.profile_loader import ProfileLoader
from commons_qa.utils.random_utils import get_random_id


class StepFunctionInvoker:
    """
    Clase encargada de conectarse con el servicio Step Function de AWS
    """
    def __init__(self, account, stepfunction_name):
        self._profile = ProfileLoader.get_profile()
        self._client = Authenticator.for_account_and_service(account, 'stepfunctions').client
        self._execution_name = 'test-sfn-execution' + get_random_id()
        account_id = self._profile.account_id(account)
        self._state_machine_arn = 'arn:aws:states:us-east-1:' + account_id + ':stateMachine:' + stepfunction_name
        self._log = logger.get_logger(__name__)

    def invoke(self, payload):
        """
        Este método inicia la ejecución de una máquina de estado

        :param payload: (string) --
                        String que contiene el JSON con los datos de entrada para la ejecución. Por ejemplo::

                            "{\"first_name\" : \"test\"}"

        :return type: dict
        :return:
            Sintáxis de la respuesta::

                {
                    'executionArn': 'string',
                    'startDate': datetime(2015, 1, 1)
                }

        Ejemplo::

            step_function_invoker = StepFunctionInvoker("prepaid", f"automatic-verification-step-env")

            payload = json.dumps({'username': 'some_username'})
            step_function_invoker.invoke(payload)

        """
        self._log.info("Starting step function execution")
        response = self._client.start_execution(
            stateMachineArn=self._state_machine_arn,
            name=self._execution_name,
            input=payload
        )
        return response

    def wait_to_finish_execution(self, execution_arn, timeout):
        """
        Este método espera el tiempo requerido a que el estado de una ejecución iniciada sea distinto de RUNNING.
        Si el estado no cambia el método devuelve una excepción

        :param execution_arn: execution_arn de la ejecución iniciada
        :param timeout: tiempo de espera máximo en segundos

        :return type: dict
        :returns:
            Sintáxis de la respuesta::

                {
                    'executionArn': 'string',
                    'stateMachineArn': 'string',
                    'name': 'string',
                    'status': 'RUNNING'|'SUCCEEDED'|'FAILED'|'TIMED_OUT'|'ABORTED',
                    'startDate': datetime(2015, 1, 1),
                    'stopDate': datetime(2015, 1, 1),
                    'input': 'string',
                    'inputDetails': {
                        'included': True|False
                    },
                    'output': 'string',
                    'outputDetails': {
                        'included': True|False
                    },
                    'traceHeader': 'string'
                }

        Ejemplo::

            step_function_invoker = StepFunctionInvoker("prepaid", "automatic-verification-step-env")

            payload = json.dumps({'username': 'some_username'})
            execution_arn = step_function_invoker.invoke(payload)['executionArn']

            step_function_invoker.wait_to_finish_execution(execution_arn, 100)

        """
        time = datetime.datetime.now().strftime('%H:%M:%S')
        self._log.info(f"Waiting until {timeout} seconds to complete step function execution, starting at {time}")
        try:
            response = polling2.poll(
                lambda: self._client.describe_execution(
                    executionArn=execution_arn
                ),
                check_success=lambda out: out['status'] != 'RUNNING',
                step=10,
                timeout=timeout,
                ignore_exceptions=(polling2.TimeoutException,)
            )
        except:
            response = self._client.describe_execution(executionArn=execution_arn)
            self._log.info(
                f"After wait for {timeout} seconds, the execution did't finish, the status is: {response['status']}")
            assert response['status'] != 'RUNNING'

        self._log.info(f"Step function execution finished with status: {response['status']}")
        return response

    def start_execution_and_wait_to_finish_with_status(self, payload, status, timeout):
        """
        Este método inicia la ejecución de una máquina de estado y espera el tiempo requerido a que el estado de la ejecución sea el indicado por parámetro.
        Si el estado no cambia el método devuelve una excepción

        :param payload: (string) -- String que contiene el JSON con los datos de entrada para la ejecución. Por ejemplo::

                            "{\"first_name\" : \"test\"}"

        :param status: (string) -- estado por el que se desea esperar ('SUCCEEDED'|'FAILED'|'TIMED_OUT'|'ABORTED')
        :param timeout: tiempo de espera máximo en segundos

        :return type: dict
        :returns:
            Sintáxis de la respuesta::

                {
                    'executionArn': 'string',
                    'stateMachineArn': 'string',
                    'name': 'string',
                    'status': 'RUNNING'|'SUCCEEDED'|'FAILED'|'TIMED_OUT'|'ABORTED',
                    'startDate': datetime(2015, 1, 1),
                    'stopDate': datetime(2015, 1, 1),
                    'input': 'string',
                    'inputDetails': {
                        'included': True|False
                    },
                    'output': 'string',
                    'outputDetails': {
                        'included': True|False
                    },
                    'traceHeader': 'string'
                }

        Ejemplo::

            step_function_invoker = StepFunctionInvoker("prepaid", "automatic-verification-step-dev")
            payload = json.dumps({'username': 'some_username'})
            step_function_invoker.start_execution_and_wait_to_finish_with_status(payload, 'SUCCEEDED', 500)

        """
        response = self.invoke(payload)
        assert response.get('ResponseMetadata').get('HTTPStatusCode') == 200
        result = self.wait_to_finish_execution(execution_arn=response['executionArn'], timeout=timeout)
        assert result['status'] == status
        return result

    def start_execution_and_wait_to_finish_with_validation(self, payload, timeout):
        """
        Este método inicia la ejecución de una máquina de estado y espera el tiempo requerido a que el estado de la ejecución sea distinto a RUNNING.
        Si el estado no cambia el método devuelve una excepción

        :param payload: (string) -- String que contiene el JSON con los datos de entrada para la ejecución. Por ejemplo::

                            "{\"first_name\" : \"test\"}"

        :param timeout: tiempo de espera máximo en segundos

        Ejemplo::

            step_function_invoker = StepFunctionInvoker("prepaid", "automatic-verification-step-dev")
            payload = json.dumps({'username': 'some_username'})
            step_function_invoker.start_execution_and_wait_to_finish_with_validation(payload, 500)

        """
        response = self.invoke(payload)
        assert response.get('ResponseMetadata').get('HTTPStatusCode') == 200
        self.wait_to_finish_execution(execution_arn=response['executionArn'], timeout=timeout)
