from src.Shorten import Shorten

class shortenSteps:
    def postShorten(self):
        url = "https://url-shortener-service.p.rapidapi.com"
        path= "/shorten"
        token = "5bc1a34c40mshe27d32ebba28f93p1f3a18jsn222d7956a53b"
        body = 'url=https://google.com/'
        return Shorten.postShorten(self, url, path, token, body)

    def postShortenUrl(self):
        url = "https://url-shortener-service.p.rapidapi.com"
        path = "/shorten"
        token = "5bc1a34c40mshe27d32ebba28f93p1f3a18jsn222d7956a53b"
        body = 'url='
        return Shorten.postShorten(self, url, path, token, body)

    def postShortenToken(self):
        url = "https://url-shortener-service.p.rapidapi.com"
        path = "/shorten"
        token = ""
        body = 'url=https%3A%2F%2Fgoogle.com'
        return Shorten.postShorten(self, url, path, token, body)