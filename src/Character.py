from commons_qa.api.api_client import ApiClient


class Character:
    def character(self, path):
        api_client = ApiClient("https://rickandmortyapi.com/api")
        headers = {
            "Content-Type": "application/json"
        }
        return api_client.get(path=path, headers=headers, params={})
