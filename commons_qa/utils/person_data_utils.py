"""
Archivo .py con la clase encargada de generar datos relacionados a una persona (DNI, CUIT, y Telefono) solo para Argentina
"""
import random


class PersonDataUtils:
    """
    Clase que permite generar datos relacionados a una persona (DNI, CUIT, y Telefono) solo para Argentina
    """

    def __init__(self):
        print()

    def get_random_personal_id(self):
        """
        Método que retorna un DNI válido para Argentina
        :return type: String
        :return: DNI válido (entre los numeros 10000000 y 45000000)
        """
        return str(random.randint(10000000, 45000000))

    def get_cuit_for(self, dni, gender):
        """
        Método que retorna un CUIT válido para Argentina

        :param dni: DNI a partir del cual se quiere generar el CUIT
        :param gender: género de la persona, indicar con M o F

        :return type: String
        :return: CUIT válido
        """
        partialCuit = self.__get_prefix_for(gender) + dni
        lastDigit = self.__get_last_digit_for(partialCuit)
        return self.__get_cuit(lastDigit, partialCuit)

    def __get_prefix_for(self, gender):
        if gender == 'M':
            prefix = "20"
        elif gender == 'F':
            prefix = "27"
        else:
            prefix = "30"  # Empresa
        return prefix

    def __get_last_digit_for(self, partial_cuit):
        multipliers = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2]
        digits = [int(x) for x in str(partial_cuit)]
        sum = 0
        for i in range(0, len(digits)):
            sum += digits[i] * multipliers[i]
        return sum % 11

    def __get_cuit(self, lastDigit, partialCuit):
        if lastDigit == 0:
            return partialCuit + "0"
        elif lastDigit == 1:
            if partialCuit[0: 2] == "20":
                return partialCuit.replace("20", "23", 1) + "9"
            else:
                return partialCuit.replace("27", "23", 1) + "4"
        else:
            return partialCuit + str(11 - lastDigit)

    def generate_random_phone(self):
        """
        Método para generar un teléfono válido para argentina

        :return type: String
        :return: Número de teléfono válido para Arg: +5491xxxxxxxx
        """
        num = str(random.randint(10000000, 99999999))
        return f'+54911{num}'
