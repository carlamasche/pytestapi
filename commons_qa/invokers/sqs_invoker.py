"""
Archivo py con la clase encargada de conectarse con el servicio SQS de AWS
"""
import json
import logging
import traceback
from json import JSONDecoder

import polling2

from commons_qa.configs.authenticator import Authenticator
from commons_qa.profiles.profile_loader import ProfileLoader


class SQSInvoker:
    """
    Esta clase contiene todos los métodos necesarios para conectarse con el servicio de Amazon Simple Queue Service (SQS).
    """
    AWS_SERVICE_NAME = 'sqs'
    ENVIRONMENT = ProfileLoader.get_profile().environment

    def __init__(self, account, sqs_url):
        self._client = Authenticator.for_account_and_service(account, self.AWS_SERVICE_NAME).client
        self._sqs_url = sqs_url
        self.logger = logging.getLogger(__name__)

    def receive_messages_with_metadata(self, wait_time_seconds=2):
        """
        Este método retorna el mensaje completo encontrado en la queue de manera aleatoria

        :param wait_time_seconds: tiempo de espera máximo en segundos hasta encontrar un mensaje, por defecto son 2 segundos
        :return: mensaje completo encontrado en la queue
        """
        return self._client.receive_message(QueueUrl=self._sqs_url, WaitTimeSeconds=wait_time_seconds)

    def receive_messages(self):
        """
        Este método retorna el body de un mensaje que encuentra de manera aleatoria en la queue

        :return: un json con el primer mensaje disponible en la queue
        """

        self.logger.info("Receiving message from sqs...")
        try:
            messages = self.receive_messages_with_metadata()['Messages']
            for msg in messages:
                msg['Body'] = json.loads(msg['Body'])
            return messages
        except Exception:
            traceback.print_exc()

    def send_message(self, payload):
        """
        Este método envía un mensaje a la queue.

        :param payload: mensaje que se desea enviar a la queue
        :return: dict
                ::

                    Sintáxis de la respuesta::
                        {
                            'MD5OfMessageBody': 'string',
                            'MD5OfMessageAttributes': 'string',
                            'MD5OfMessageSystemAttributes': 'string',
                            'MessageId': 'string',
                            'SequenceNumber': 'string'
                        }

        """

        self.logger.info("Sending message to sqs...")
        response = self._client.send_message(
            QueueUrl=self._sqs_url,
            MessageBody=payload
        )
        return response

    def delete_all_elements_in_queue(self):
        """
        Este método elimina todos los elementos en la queue.
        """

        try:
            return self._client.purge_queue(
                QueueUrl=self._sqs_url
            )
        except Exception as e:
            if 'AWS.SimpleQueueService.PurgeQueueInProgress' in e.response.get('Error', {}).get('Code', ''):
                pass
            else:
                raise e

    def get_all_message_in_sqs(self):
        """
        Este método retorna una lista de todos los mensajes encontrados en la queue

        :return: lista con el body de todos los mensajes
        """

        self.logger.info("Receiving all messages from sqs...")
        messages = self.receive_messages()
        all_messages = []
        while messages is not None:
            for message in messages:
                all_messages.append(message)
            messages = self.receive_messages()
        return all_messages

    def get_all_queue_attributes(self):
        """
        Este método trae todos los atributos de la queue.
        """

        return self._client.get_queue_attributes(
            QueueUrl=self._sqs_url, AttributeNames=['ApproximateNumberOfMessagesDelayed']
        )

    def find_message_with_polling_in_filter(self, keys, value, timeout):
        """
        Este método devuelve el mensaje completo en el Body de una SQS en donde la key provista tiene el valor indicado.
        Se puede pasar una key y un value, o un array con una sucesión de keys y el valor buscado.

        :param keys: puede ser un string o una lista de strings con la key del valor buscado
        :param value: el valor que tiene asignado una key o sucesión de keys
        :param timeout: tiempo maximo de busqueda del mensaje

        :return: Devuelve el mensaje encontrado, si no es encontrado devuelve None

        Por ejemplo, dado un json con esta estructura::

               {
                   "Id" : "50fb6ef3-c78e-a884-418f-fa99ecc8bdf9" ,
                    "EventType" : "ACCOUNT_ADJUSTMENT" ,
                    "Payload":
                        {
                            "AccountId":"70772087-cb9c-487c-bb45-8c3a6fb72183",
                            "Amount":420
                        }
                }

        Podríamos pasar como parámetros:
            keys=Id, value=50fb6ef3-c78e-a884-418f-fa99ecc8bdf9

        O también:
            keys=[Payload, Amount], value=420
        """

        self.logger.info(f"Getting messages from sqs according to the keys {keys} and value {value}...")
        try:
            result = polling2.poll(
                lambda: self.receive_messages(),
                check_success=lambda out: self.__message_contain(response=out, keys=keys, value=value),
                step=1,
                timeout=timeout,
                ignore_exceptions=(polling2.TimeoutException,)
            )
            return JSONDecoder().decode(result[0]['Body']['Message'])
        except:
            return None

    def __message_contain(self, response, keys, value):
        """
        Este método devuelve el contenido del Body de un mensaje dentro de una SQS.
        Se puede pasar una key y un value para identificar el mensaje del que queremos obtener el body.
        """

        if response is not None:
            json_body_message = json.loads(response[0].get("Body").get("Message"))
            try:
                for key in keys:
                    json_body_message = json_body_message.get(key)
                self.logger.info(json_body_message)
                return json_body_message == value
            except:
                return False
        else:
            return False
