import pytest
import allure
from steps.Character_Steps import character_steps


class TestCharacter:

    pytestmark = [
        allure.suite("RickYMorty"),
    ]

    @pytest.mark.rickAndMorty
    def test_get_character(self):
        response = character_steps.getCharacter(self)
        print(response.json())

        assert (
            response.status_code == 200
        ), f"Response was not successful, status [{response.status_code}], body [{response.json()}]"

        assert response.json()["name"] == 'Morty Smith'
