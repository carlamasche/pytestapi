"""
Archivo .py con la clase encargada de encodear diccionarios
"""
from json import JSONEncoder


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__
