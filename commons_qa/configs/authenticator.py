"""
Archivo py con las clases encargadas de autenticarse al cliente de boto3
"""
import os

import boto3
from commons_qa.profiles.profile_loader import ProfileLoader


class Authenticator:
    """
    Clase encargada de autenticarse al cliente de boto3 según el role del usuario
    """
    @classmethod
    def for_account_and_service(cls, account, service_name):
        _role = os.getenv('role')
        profile = ProfileLoader.get_profile()

        if _role == "ci":
            return RoleBasedAuthenticator(profile, account, service_name)
        else:
            return ProfileBasedAuthenticator(profile, account, service_name)

    @property
    def client(self):
        return self._client

    @property
    def session(self):
        return self._session


class ProfileBasedAuthenticator(Authenticator):
    """
    Clase encargada de autenticarse al cliente de boto3 con credenciales locales
    """
    def __init__(self, profile, account, service_name):
        profile_name = profile.get_profile_name(account)
        self._session = boto3.Session(profile_name=profile_name, region_name=profile.region)
        if service_name:
            self._client = self._session.client(service_name)


class RoleBasedAuthenticator(Authenticator):
    """
    Clase encargada de autenticarse al cliente de boto3 con el rol de CI
    """
    def __init__(self, profile, account, service_name):
        base_credentials_session = boto3.Session(region_name=profile.region)
        role_arn = profile.ci_role(account)
        sts_credentials = base_credentials_session.client('sts', region_name=profile.region).assume_role(
            RoleArn=role_arn,
            RoleSessionName='iam_r_cross_account_testing',
            DurationSeconds=3600
        )
        self._session = boto3.Session(
            aws_access_key_id=sts_credentials['Credentials']['AccessKeyId'],
            aws_secret_access_key=sts_credentials['Credentials']['SecretAccessKey'],
            aws_session_token=sts_credentials['Credentials']['SessionToken']
        )
        if service_name:
            self._client = self._session.client(service_name, region_name=profile.region)
