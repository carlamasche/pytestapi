"""
Archivo py con la clase encargada de conectarse con el servicio SNS de AWS
"""
import logging
import random
import traceback
from commons_qa.configs.authenticator import Authenticator
from commons_qa.profiles.profile_loader import ProfileLoader



class SNSInvoker:
    """
    Clase encargada de conectarse con el servicio Single Notification Service de AWS
    """
    
    def __init__(self, account_name, topic, region='us-east-1', filter_policy=None, with_queue=True):
        """
        Constructor
        :param account_name: Nombre de la cuenta de AWS donde se encuentra la SNS
        :param topic: El nombre del tópico de la SNS
        :param region: (opcional) La region de la cuenta en la que se encuentra la SNS
        :param filter_policy: (opcional)
        :param with_queue: (opcional)
            True(default): Crea una cola SQS y la suscribe al SNS
            False: NO crea ni suscribe ninguna SQS.


        Ej: SNSInvoker('bancar', f'SalesforceMirroring-topic-dev')

       """

        self.profile = ProfileLoader().get_profile()
        self.logger = logging.getLogger(__name__)
        self.account_name = account_name
        self.account = self.profile.account_id(account_name)
        self.region = region
        self.topic_name = topic
        self.logger.info('using aws account: {0}'.format(self.account))
        self.sqs_client = Authenticator.for_account_and_service(account_name, 'sqs').client
        self.sns_invoker = Authenticator.for_account_and_service(account_name, 'sns').client
        self.topic_arn = 'arn:aws:sns:{0}:{1}:{2}'.format(self.region, self.account, self.topic_name)
        self.logger.info('using topic arn: {0}'.format(self.topic_arn))
        self.filter_policy = filter_policy
        if with_queue:
            self.queue = self.create_and_subscribe_temp_queue()

    def create_and_subscribe_temp_queue(self):
        """
        Crea una SQS Temporal y la suscribe a la SNS
        :return: la URL de la queue creada
        """
        temp_queue = self.__create_temp_queue()
        temp_queue.subscription_arn = self.__subscribe_queue_to_sns_topic(temp_queue)
        return temp_queue

    def delete_and_unsubscribe_sqs(self, queue):
        """
        Borra la cola SQS
        :param queue: Una instancia de la cola a borrar y desuscribir.
        :return:
        """
        try:
            self.logger.info('Unsubscribe & delete queues')
            self.sns_invoker.unsubscribe(SubscriptionArn=queue.subscription_arn)
            self.sqs_client.delete_queue(QueueUrl=queue.url)
        except Exception:
            self.logger.error('There was an error deleting queue!')
            traceback.print_exc()

    def __del__(self):
        """
        Destructor. Este método se invoca automaticamente una vez que el objeto se destruye.
        :return:
        """
        self.delete_and_unsubscribe_sqs(self.queue)

    # Métodos auxiliares

    def __create_temp_queue(self):
        """
        Este método crea una queue temporal con el fin de suscribir a una SNS que se le indique.

        Ejemplo::

            sns_listener.create_temp_queue()

        """
        queue_name = 'TempQueue_' + self.topic_name + '_' + str(random.randint(1, 10000))
        sqs_data = self.sqs_client.create_queue(QueueName=queue_name)
        queue_url = sqs_data['QueueUrl']
        self.logger.info('queue url: {0}'.format(queue_url))
        queue_arn = 'arn:aws:sqs:{0}:{1}:{2}'.format(self.region, self.account, queue_name)
        self.logger.info('queue arn: {0}'.format(queue_arn))
        temp_queue = Queue(queue_arn, queue_url)
        return temp_queue

    def __subscribe_queue_to_sns_topic(self, queue):
        """
        Este método suscribe la queue temporal al topic que se le indique.

        Ejemplo::

            sns_listener.subscribe_queue_to_sns_topic()

        """
        attributes_values = {'FilterPolicy': self.filter_policy} if self.filter_policy is not None else {}
        subscription_arn = self.sns_invoker.subscribe(
            TopicArn=self.topic_arn,
            Protocol='sqs',
            Endpoint=queue.arn,
            Attributes=attributes_values,
            ReturnSubscriptionArn=True
        )["SubscriptionArn"]
        self.logger.info('subscription arn: {0}'.format(subscription_arn))
        policy_json = self.allow_sns_to_write_to_sqs(self.topic_arn, queue.arn)
        self.sqs_client.set_queue_attributes(
            QueueUrl=queue.url,
            Attributes={
                'Policy': policy_json
            }
        )

        return subscription_arn

    def allow_sns_to_write_to_sqs(self, topic_arn, queue_arn):
        """
        Este metodo permite que la SNS inserte informacion en la queue SQS
        """

        policy_document = """{{
              "Version":"2012-10-17",
              "Statement":[
                {{
                  "Sid":"MyPolicy",
                  "Effect":"Allow",
                  "Principal" : {{"AWS" : "*"}},
                  "Action":"SQS:SendMessage",
                  "Resource": "{}",
                  "Condition":{{
                    "ArnEquals":{{
                      "aws:SourceArn": "{}"
                    }}
                  }}
                }}
              ]
            }}""".format(queue_arn, topic_arn)
        return policy_document

      
class Queue:

    def __init__(self, arn, url, subscription_arn=None):
        self.arn = arn
        self.url = url
        self.subscription_arn = subscription_arn

