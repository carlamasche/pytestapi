from commons_qa.api.api_client import ApiClient
from commons_qa.invokers.secrets_manager_invoker import SecretsManagerInvoker


class AmplitudeApi:
    """
    AmplitudeApi se encarga de obtener los eventos de los usuarios en amplitude
    NOTA = La respuesta de la API tiene mucha informacion sobre muchas cosas (como sea desde que dispositivo
    conecto, hasta la fecha y hora exacta en que se levanta algun evento) por eso sera responsabilidad de cada
    equipo parsear los datos que devuelve segun lo que necesiten.
    """

    # TODO: falta agregar credenciales 'api_key' y 'secret_key' en github secrets
    def __init__(self):
        self.api_url = 'https://amplitude.com/api/2'
        self.secret_manager = SecretsManagerInvoker('bancar').get_secret_value('PROD/apiKeyAmplitude')
        self.amplitude_api_key = self.secret_manager['api_key']
        self.amplitude_secret_key = self.secret_manager['secret_key']
        self.api_client = ApiClient(api_url=self.api_url,
                                    auth=(self.amplitude_api_key, self.amplitude_secret_key))
        self.current_account_id = None
        self.current_amplitude_id = None

    def get_amplitude_id(self, account_id):
        """
        Request que busca segun el AccountId el amplitude_id que necesitamos para hacer
        los request de Useractivity y demas de la api de Amplitude
        Los datos del account id y amplitude id son guardados una vez realizada la llamada
        con el objetivo de reutilizar los valores obtenidos previamente
        En el caso de que sea un nuevo account id, el amplitude id será actualizado
        :param account_id = string, AccountId de dyanamo de el usuario
        :return: el amplitud_id del usuario
        """
        if self.current_amplitude_id is None or self.current_account_id != account_id:
            path = f'/usersearch?user={account_id}'
            response = self.api_client.get(path=path)
            amplitude_id = response.json()['matches']
            if len(amplitude_id) == 0:
                raise ValueError('The AccountID did not match with any user in Amplitude')
            else:
                self.current_account_id = account_id
                self.current_amplitude_id = amplitude_id[0]['amplitude_id']
        return self.current_amplitude_id

    def user_activity_from_amplitude(self, account_id):
        """
        Request que devuelve los todos los eventos de amplitude del usuario desde su creacion
        :param account_id = string, account_id del usuario
        :return obj response del endpoint
        """
        amplitude_id = self.get_amplitude_id(account_id)
        path = f'/useractivity?user={amplitude_id}'
        response = self.api_client.get(path=path)
        return response.json()

    def lasts_events_from_user_activity(self, account_id, last_events=10):
        """
        Request que devuelve los ultimos eventos limitados por el param 'last_events'.
        Por defecto esta en 10, esto devuelve los ultimos 10 eventos del usuario.
        :param account_id string, AccountId del usuario
        :param last_events = int, numero de los ultimos eventos que se quiera ver
        :return obj response del endpoint
        """
        amplitude_id = self.get_amplitude_id(account_id)
        path = f'/useractivity?user={amplitude_id}&limit={last_events}'
        response = self.api_client.get(path=path)
        return response.json()['events']
