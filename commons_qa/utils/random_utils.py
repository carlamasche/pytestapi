"""
Archivo .py con funciones para generar ids
"""
import uuid


def get_random_id():
    """
    Método que retorna una string con un id con las mismas características que los utilizados por ejemplo como index en DynamoDB.

    :return type: String
    Ejemplo::

        28afebc2-c3b7-4d72-932e-1a3b789d3041
    """
    return str(uuid.uuid4())
