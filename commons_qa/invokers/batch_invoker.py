"""
Archivo py con la clase encargada de manejar peticiones a procesos Batch
"""
import logging

import polling2
from commons_qa.configs.authenticator import Authenticator
from commons_qa.utils.validation_utils import batch_amount_of_job_status_to_be_more_than


class BatchInvoker:
    """
    Clase encargada de manejar peticiones a procesos Batch

    """
    AWS_SERVICE_NAME = 'batch'

    def __init__(self, account):
        authenticator = Authenticator.for_account_and_service(account, self.AWS_SERVICE_NAME)
        self._client = authenticator.client
        self.logger = logging.getLogger(__name__)

    def wait_for_last_job_in_status(self, job_queue, status, previous_amount_of_job_with_status, timeout):

        """
        Este método busca el último batch que se ejecuto y lo trae

        :param job_queue: Nombre de la queue en la que se quiere buscar.
        :param status: Estados posibles de la queue (SUBMITTED,PENDING, RUNNABLE, STARTING, RUNNING, SUCCEEDED, FAILED).
        :param previous_amount_of_job_with_status: Devuelve la cantidad de trabajos en la queue.
        :param timeout: Tiempo permitido de demora en ejecución.
        :return type: dict
        :return:

            Sintáxis de la respuesta::

                {
                    'jobSummaryList': [
                        {
                            'jobArn': 'string',
                            'jobId': 'string',
                            'jobName': 'string',
                            'createdAt': 123,
                            'status': 'SUBMITTED'|'PENDING'|'RUNNABLE'|'STARTING'|'RUNNING'|'SUCCEEDED'|'FAILED',
                            'statusReason': 'string',
                            'startedAt': 123,
                            'stoppedAt': 123,
                            'container': {
                                'exitCode': 123,
                                'reason': 'string'
                            },
                            'arrayProperties': {
                                'size': 123,
                                'index': 123
                            },
                            'nodeProperties': {
                                'isMainNode': True|False,
                                'numNodes': 123,
                                'nodeIndex': 123
                            },
                            'jobDefinition': 'string'
                        },
                    ],
                    'nextToken': 'string'
                }
        """

        self.logger.info("Checking job status {} on jobQueue  {} ...".format(status, job_queue))
        result = polling2.poll(
            lambda: self._client.list_jobs(jobQueue=job_queue, jobStatus=status),
            check_success=lambda out: batch_amount_of_job_status_to_be_more_than(
                                                                    amount=previous_amount_of_job_with_status,
                                                                    response=out),
            step=10,
            timeout=timeout,
            ignore_exceptions=(polling2.TimeoutException,)
        )
        return result

    def current_amount_of_job_with_status(self, job_queue, status):

        """
        Este método devuelve la cantidad actual de trabajos en la queue indicada.

        :param job_queue: Nombre de la queue en la que se quiere buscar.
        :param status: Status utilizado para filtrar la búsqueda.
        :return type: int
        """

        self.logger.info("Getting amount of job with status {} on jobQueue  {} ...".format(status, job_queue))
        response = self._client.list_jobs(jobQueue=job_queue, jobStatus=status)
        return len(list(response['jobSummaryList']))
