"""
Archivo .py con funciones para llamar a lambdas asincronicamente
"""
import polling2
import logging


def call_lambda_async(lambda_function, timeout=120, step=10, check_success=polling2.is_truthy, ignore_exceptions=()):
    """
    Se invoca a una función objetivo hasta que se cumpla una determinada condición.
    Debe especificar al menos una funcion objetivo a llamar.

    :param lambda_function: La funcion objetivo a invocar

    :param timeout: Se llamará a la función de objetico hasta que el tiempo transcurrido sea mayor que el tiempo de espera máximo (en segundos).

    :param step: Define el monto de tiempo a esperar (en segundos)

    :param check_success: - Una función callback que acepta el valor de retorno de la función objetivo.

                          - Debería devolver verdadero si desea que la función de sondeo se detenga y devuelva este valor.

                          - Debería devolver falso si se desea continuar ejecutando.

                          - El valor predeterminado es un callback que prueba veracidad (cualquier cosa que no sea Falso, 0 o una coleccion vacia).

    :param ignore_exceptions: Tupla, Puede especificar una tupla de excepciones que deben detectarse e ignorarse en cada iteración.

    :return type: Boolean
    :return:

        Verdadero: En caso que la funcion lambda devuela True dentro del timeout establecido

        Falso: En caso que la funcion lambda NO devuelva True dentro del timeout establecido
    """

    logger = logging.getLogger(__name__)
    logger.info('running {} with timeout: {} and step: {}'.format(lambda_function.__name__, timeout, step))

    return polling2.poll(
        lambda_function,
        check_success=check_success,
        timeout=timeout,
        step=step,
        ignore_exceptions=ignore_exceptions)