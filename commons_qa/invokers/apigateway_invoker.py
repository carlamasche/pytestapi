"""
Archivo py con la clase encargada de manejar peticiones a api-rest privadas
"""
import logging
from commons_qa.configs.authenticator import Authenticator


class ApiGatewayInvoker:
    """
    Clase encargada de manejar peticiones a api-rest privadas
    """
    AWS_SERVICE_NAME = 'apigateway'

    def __init__(self, account):
        authenticator = Authenticator.for_account_and_service(account, self.AWS_SERVICE_NAME)
        self._client = authenticator.client
        self.logger = logging.getLogger(__name__)

    def test_invoke_method(self, rest_api_id, resource_id, http_method, body, headers={}, path_with_query_string=""):

        """
        Este método invoca a la api privada de aws que especifiquemos

            :param rest_api_id: Id del servicio al que se le quiere peticionar (lo sacamos de aws)
            :param resource_id:  Id del endpoint al que se le quiere peticionar (lo sacamos de aws)
            :param http_method:  Método HTTP a utilizar (GET, POST, PUT, DELETE, UPDATE, PATCH)
            :param body: cuerpo del request en formato dic, json
            :param headers: (opcional)
            :param path_with_query_string: (opcional) Envía información en el path (un ID en un get by ID)
            :return type: Response

        Ejemplo de uso::

            payload = {
                  "balance": 891428583106,
                  "cb_transaction_id": "2639",
                  "status": "authorized"
            }

            response = self.apigw_invoker.test_invoke_method(
                rest_api_id=self.__apigateway_id,
                resource_id=self.__resource_id,
                http_method="POST",
                body=json.dumps(payload)
            )
        """

        api_response = self._client.test_invoke_method(
            restApiId=rest_api_id,
            resourceId=resource_id,
            httpMethod=http_method,
            body=body,
            headers=headers,
            pathWithQueryString=path_with_query_string
        )
        return api_response
