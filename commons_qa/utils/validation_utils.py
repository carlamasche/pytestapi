"""
Archivo .py con funciones utilizadas para hacer verificaciones en polling
"""


def items_not_empty(response):
    return len(response['Items']) != 0


def items_greater_than_one(response):
    return len(response['Items']) > 1


def messages_not_empty(response):
    return 'Messages' in response


def messages_are_empty(response):
    return not messages_not_empty(response)


def status_equal_to(status, response):
    return response['Items'] and response['Items'][0]['Status']['S'] == status


def status_not_equal_to(status, response):
    return response['Items'] and response['Items'][0]['Status']['S'] != status


def status_not_equal_to_created(response):
    return status_not_equal_to('CREATED', response)


def status_not_equal_to_authorized(response):
    return status_not_equal_to('AUTHORIZED', response)


def status_equal_to_authorized(response):
    return status_equal_to('AUTHORIZED', response)


def status_equal_to_pending_compliance_validation(response):
    return status_equal_to('PENDING_COMPLIANCE_VALIDATION', response)


def status_equal_to_valid(response):
    return status_equal_to('VALID', response)


def status_equal_to_canceled(response):
    return status_equal_to('CANCELED', response)


def status_equal_to_processed(response):
    return status_equal_to('PROCESSED', response)


def status_equal_to_reversed(response):
    return status_equal_to('REVERSED', response)


def bancar_status_equal_to_canceled(response):
    return status_equal_to('CANCELED', response)


def status_equal_to_authorized_canceled(response):
    return status_equal_to('AUTHORIZED_CANCELED', response)


def status_equal_to_not_authorized(response):
    return status_equal_to('NOT_AUTHORIZED', response)


def status_equal_to_pending_validation(response):
    return status_equal_to('PENDING_VALIDATION', response)


def status_equal_to_blacklisted(response):
    return status_equal_to('BLACKLISTED', response)


def balance_amount_changed(old_balance_amount, response=None):
    return response['Items'] and not float(response['Items'][0]['AvailableBalance']['N']) == float(old_balance_amount)


def balance_date_changed(old_balance_date, response=None):
    return response['Items'] and response['Items'][0]['UpdatedDate']['S'] > old_balance_date


def balance_amount_changed_core(old_balance_amount, response=None):
    return response['Items'] and response['Items'][0]['AvailableBalance']['N'] != old_balance_amount


def two_transaction_with_same_external_id(response):
    return response['Items'] and len(response['Items']) == 2


def rds_response_not_empty(response):
    return response is not None


def rds_items_not_empty(response):
    return len(response) != 0


def batch_amount_of_job_status_to_be_more_than(amount, response):
    return len(list(response['jobSummaryList'])) > amount
