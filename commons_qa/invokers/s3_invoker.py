"""
Archivo py con la clase encargada de conectarse con el servicio de almacenamiento de objetos S3 de AWS
"""
from commons_qa import logger

from botocore.exceptions import ClientError

from commons_qa.configs.authenticator import Authenticator


class S3Invoker:
    """
    Esta clase contiene todos los métodos necesarios para conectarse con el servicio de almacenamiento de objetos S3 de AWS.
    """
    AWS_SERVICE_NAME = 's3'

    def __init__(self, account):
        authenticator = Authenticator.for_account_and_service(account, self.AWS_SERVICE_NAME)
        self._client = authenticator.client
        self._session = authenticator.session
        self.logger = logger.get_logger(__name__)

    def upload_file(self, file_name, bucket, object_name=None, extra_args=None):
        """
        Este método sube un archivo a un bucket de S3.

            :param file_name: Nombre del archivo a subir.
            :param bucket: Bucket al que se sube el archivo.
            :param object_name: Nombre del archivo convertido a objeto S3 (de no especificarse se toma file_name).
            :param extra_args: Argumentos extra, tales como ACL, ContentType u otra metadata del archivo.

            :return: True si el archivo fue subido, sino False.

            Ejemplo::

                upload_file(file_name= 'prepaid',
                                  bucket= 'uala-arg-prepaid-ejemplo-files-{}',
                                  object_name=None,
                                  extra_arg={'ACL': 'private'})
        """

        if extra_args is None:
            extra_args = {'ACL': 'public-read'}
        # If S3 object_name was not specified, use file_name
        if object_name is None:
            object_name = file_name

        # Upload the file
        try:
            self._client.upload_file(file_name, bucket, object_name, ExtraArgs=extra_args)
        except ClientError as e:
            self.logger.error(e)
            return False
        return True

    def delete_file(self, bucket, filename):
        """
        Este método elimina un archivo de un bucket.

            :param bucket: Nombre del bucket del que se eliminará el archivo.
            :param filename: Nombre del archivo.

            Ejemplo::

                delete_file(file_name= 'prepaid',
                                  bucket= 'uala-arg-prepaid-ejemplo-files-{}')
        """
        s3 = self._session.resource('s3')
        s3.Object(bucket, filename).delete()

    def head_object(self, bucket, key):
        """
        Este método devuelve metadata de un objeto S3.

            :param bucket: Nombre del bucket.
            :param key: Key del objeto S3.

            Ejemplo::

                head_file(bucket= 'uala-arg-prepaid-ejemplo-files-{}',
                                  key=(account='prepaid'))
        """
        try:
            return self._client.head_object(Bucket=bucket, Key=key)
        except ClientError as exc:
            if exc.response['Error']['Code'] != '404':
                raise

    def upload_fileobj(self, filepath, bucket_name, object_key, extr_args=None, callback=None, config=None):
        """
        Este método sube un objeto-archivo bucket. El objeto-archivo debe estar en modo binario.

            :param filepath: Objeto-archivo a subir.
                Como mínimo, debe implementar el método de lectura y debe retornar bytes.
            :param bucket_name: (str) - Nombre del bucket al que se va a subir.
            :param object_key: (str) - Nombre del bucket al que se va a subir.
            :param extr_args: (dict) - Argumentos adicionales.
            :param callback: (function) - Número de bytes transferidos durante la carga.
            :param config: (boto3.s3.transfer.TransferConfig) - Configuración de la carga.

            Ejemplo::

                upload_fileobj(file_path= 'arn:aws:batch:{0}:{1}:job-queue/test_files/ejemplo_model.csv",
                                  bucket_name= 'uala-arg-prepaid-ejemplo-files-{}',
                                  object_key='ejemplo/test-qa.csv')
        """
        with open(filepath, 'rb') as data:
            self._client.upload_fileobj(data, bucket_name, object_key)

    def delete_object(self, bucket_name, object_key):
        """
        Este método elimina un objeto de un bucket.

            :param bucket_name: Nombre del bucket del que se eliminará el objeto.
            :param object_key: Key del objeto a eliminar.
            :return type: dict
            :return:

                Sintáxis de la respuesta::

                        {
                            'DeleteMarker': True|False,
                            'VersionId': 'string',
                            'RequestCharged': 'requester'
                        }

            Ejemplo::

                delete_object(bucket_name= 'uala-arg-prepaid-ejemplo-files-{}',
                                  object_key='ejemplo/test-qa.csv')
        """
        return self._client.delete_object(Bucket=bucket_name, Key=object_key)

    def download_object(self, bucket_name, key, filename, extra_args=None, callback=None):
        """
        Este método descarga un objeto de un bucket.

            :param bucket_name: Bucket del que se descargará.
            :param key: Key del objeto.
            :param filename: Nombre y path del archivo a descargar.
            :param extra_args: Argumentos extra, tales como ACL, ContentType u otra metadata del archivo.
            :param callback: Función a ser llamada cuando el archivo se haya descargado.

            Ejemplo::

                download_object(bucket_name= 'uala-arg-prepaid-ejemplo-files-{}',
                                  key='ejemplo/test-qa.csv',
                                  file_name= 'prepaid',
                                  extra_args=None,
                                  callback=None)
        """
        self._client.download_file(bucket_name, key, filename, extra_args, callback)

    def list_objects_v2(self, bucket_name, **kwargs):
        """
        Este método lista los archivos almacenados en un bucket dado.
        Referencia Boto3: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html?highlight=list%20objects#S3.Client.list_objects_v2

            :param bucket_name: (str) - Nombre del bucket.
            :param kwargs: Parámetros adicionales de la búsqueda.
            :return type: dict

             Ejemplo::

                list_object_v2(bucket_name= 'uala-arg-prepaid-ejemplo-files-{}',
                                **kwargs='ejemplo/test-qa.csv')
        """
        return self._client.list_objects_v2(Bucket=bucket_name, **kwargs)

    def get_object(self, bucket_name, **kwargs):
        """
         Este método trae información de un objeto específico de un bucket dado.
         Boto3 reference: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html?highlight=list%20objects#S3.Client.get_object

            :param bucket_name: (str) - Nombre del bucket.
            :param kwargs: Parámetros adicionales de la búsqueda.
            :return type: dict

            Ejemplo::

                get_object(bucket_name= 'uala-arg-prepaid-ejemplo-files-{}',
                            **kwargs='ejemplo/test-qa.csv')
         """
        return self._client.get_object(Bucket=bucket_name, **kwargs)
