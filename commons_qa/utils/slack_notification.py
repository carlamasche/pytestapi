import logging
from commons_qa.api.api_client import ApiClient


class SlackNotifications:

    """
    Clase encargada de enviar mensajes a un webhook previamente creado.
    Desde esta url se puede crear las apps "https://api.slack.com/apps"
    """

    def __init__(self, path_webhook):
        """
        Inicializamos la clase con la url base para todos los webhaook.
        :param path_webhook: El path del webhook donde queremos enviar los mensajes. Una vez creada la app se puede
                      encontrar la url y el path dentro de "Features/Incoming WebHooks"
        """
        self._url = "https://hooks.slack.com/services"
        self._path = path_webhook
        self._api_client = ApiClient(self._url + path_webhook)

    def post_message_to_slack(self, message):
        """
        Creamos el POST con el mensaje a enviar al webhook. En caso de error tira un error del tipo *ConnectionError*
        :param message: Obj a enviar como payload
                        Ejemplo : {"text": STRING-QUE-SE-VERA-EN-SLACK}
                        Documentacion para crear msg enriquecidos para slack https://api.slack.com/messaging/composing/layouts
        :return Obj de la respuesta del POST.
        """
        payload = message
        response = self._api_client.post(payload=payload)
        if not response.ok:
            logging.error(f"Response error trying to send a message")
            raise ConnectionError(f"Response endpoint with status code {response.status_code},"
                                  f"Response content {response.content}")
        return response
