"""
Archivo py que contiene las clases para gestionar los perfiles
"""


class Profile:
    """
    Clase que permite gestionar los perfiles SSO
    """
    @classmethod
    def for_env(cls, env_name):
        """
        Genera un objeto con los atributos correspondientes al ambiente pasado por parámetro

        :param env_name: nombre del ambiente (dev, test, stage)
        :return: Un objeto con los atributos del ambiente, en el caso de que no exista el ambiente requerido devuelve por
                defecto al ambiente dev
        """
        return next(
            (profile for profile in cls.__subclasses__() if profile.suits_env(env_name)),
            ProfileDev
        )()

    @classmethod
    def suits_env(cls, name):
        return name == cls.env()

    @classmethod
    def env(cls):
        raise NotImplementedError

    bancar_profile_name = 'uala-arg-bancar'
    bancar_account_id = '279422317367'

    ci_role_names: dict

    def ci_role(self, account_name):
        if account_name in self.ci_role_names:
            role_name = self.ci_role_names[account_name]
        else:
            role_name = f"iam_r_uala_arg_{account_name}_{self.__class__.env()}_regression_test"
        return f"arn:aws:iam::{self.account_id(account_name)}:role/{role_name}"

    def account_id(self, account_name):
        return self.__getattribute__(account_name + "_account_id")

    def get_profile_name(self, account_name):
        if hasattr(self, f"{account_name}_profile_name"):
            return self.__getattribute__(f"{account_name}_profile_name")
        else:
            return f"uala-arg-{account_name}-{self.__class__.env()}"


class ProfileDev(Profile):
    """
    Clase que genera un objeto con los atributos necesarios para el ambiente DEV
    """
    @classmethod
    def env(cls):
        return 'dev'

    environment = 'DEV'
    core_account_id = '645342462303'
    scoring_process_account_id = '695389056675'
    debit_mex_account_id = '866177945215'
    prepaid_account_id = '901301120555'
    loans_account_id = '411387832012'
    loans_mex_account_id = '508744673238'
    collections_account_id = '146225626944'
    savings_account_id = '011586644175'
    exchange_account_id = '300151377842'
    adquirencia_account_id = '748854149601'
    bo_mex_account_id = '570217591469'
    playground_account_id = '161142984839'
    debit_col_account_id = '276216885782'
    perfilamiento_account_id = '310915419691'
    core_col_account_id = '944823930735'
    engagement_arg_account_id = '409402237493'
    cxapp_arg_account_id = '018433462430'
    cxapp_mex_account_id = '779859856767'
    cxapp_account_id = '778614713785'
    bank_debit_mex_account_id = '642802187635'
    platform = '130589314549'
    platform_mex_account_id = '961847350600'
    platform_mex_bank_account_id = '842313915109'
    platform_arg_account_id = '130589314549'
    platform_arg_bank_account_id = '332625595892'
    platform_col_account_id = '774458914021'
    adquirencia_mex_account_id = '389671212122'
    remittances_mex_account_id = '696308588956'
    adquirencia_col_account_id = '733761689553'
    platform_arg_bank_account_id = '332625595892'
    debit_arg_bank_account_id = '148893621635'
    core_arg_bank_account_id = '023075686289'


    region = 'us-east-1'

    debit_mex_profile_name = 'uala-mex-debit-dev'
    scoring_process_profile_name = 'uala-arg-scoring-process-dev'
    bo_mex_profile_name = 'uala-mex-backoffice-dev'
    debit_col_profile_name = 'uala-col-debit-dev'
    core_col_profile_name = 'uala-col-core-dev'
    loans_mex_profile_name = 'uala-mex-loans-dev'
    engagement_arg_profile_name = 'uala-arg-engagement-dev'
    cxapp_arg_profile_name = 'uala-arg-cxapp-dev'
    cxapp_mex_profile_name = 'uala-mex-cxapp-dev'
    cxapp_profile_name = 'uala-cxapp-dev'
    bank_debit_mex_profile_name = 'uala-mex-bank_debit-dev'
    platform_col_profile_name = 'uala-col-platform-dev'
    platform_arg_profile_name = 'uala-arg-platform-dev'
    platform_arg_bank_profile_name = 'uala-arg-bank_platform-dev'
    platform_mex_profile_name = 'uala-mex-platform-dev'
    platform_mex_bank_profile_name = 'uala-mex-bank_platform-dev'
    adquirencia_mex_profile_name = 'uala-mex-adquirencia-dev'
    remittances_mex_profile_name = 'uala-mex-remittances-dev'
    adquirencia_col_profile_name = 'uala-col-adquirencia-dev'
    platform_arg_bank_profile_name = 'uala-arg-bank_platform-dev'
    debit_arg_bank_profile_name = 'uala-arg-bank_debit-dev'
    core_arg_bank_profile_name = 'uala-arg-bank_core-dev'


    ci_role_names = {
        'prepaid': 'iam_r_uala_arg_prepaid_dev_github_actions',
        'core': 'iam_r_uala_arg_core_dev_regression',
        'bancar': 'iam_r_uala_arg_prepaid_cross_account_github_prepaid',
        'loans': 'iam_r_uala_arg_loans_dev_github_actions',
        'loans_mex': 'iam_r_uala_mex_loans_dev_github_actions',
        'debit_mex': 'iam_r_uala_mex_debit_dev_regression_test',
        'adquirencia': 'iam_r_uala_arg_adquirencia_dev_regression',
        'scoring_process': 'iam_r_uala_arg_scoring_process_dev_github_actions',
        'bo_mex': 'iam_r_uala_mex_backoffice_dev_regression_test',
        'playground': 'iam_r_uala_arg_playground_dev_qa_regression_tests',
        'debit_col': 'iam_r_debit_regression',
        'core_col': 'iam_r_core_regression',
        'perfilamiento': 'iam_r_uala_arg_profilementfraud_dev_regressions',
        'engagement_arg': 'iam_r_uala_arg_engagement_dev_github_actions',
        'cxapp_arg': 'iam_r_uala_arg_cxapp_dev_github_actions',
        'cxapp_mex': 'iam_r_uala_mex_cxapp_dev_github_actions',
        'cxapp': 'iam_r_uala_global_cxapp_dev_github_actions',
        'bank_debit_mex': 'iam_r_uala_mex_bank_debit_dev_github_actions',
        'platform_col': 'iam_r_platform_regression',
        'platform_mex': 'iam_r_platform_regression',
        'platform_arg': 'iam_r_platform_regression',
        'platform_arg_bank': 'iam_r_platform_regression',
        'platform_mex_bank': 'iam_r_uala_mex_bank_platform_dev_github_actions',
        'adquirencia_mex': 'iam_r_uala_mex_adquirencia_dev_github_actions',
        'remittances_mex': 'iam_r_uala_mex_remittances_dev_github_actions',
        'adquirencia_col': 'iam_r_uala_col_adquirencia_dev_github_actions',
        'platform_arg_bank': 'iam_r_bank_platform_regression',
        'debit_arg_bank': 'iam_r_debit_regression',
        'core_arg_bank': 'iam_r_bank_core_regression'
    }


class ProfileTest(Profile):
    """
    Clase que genera un objeto con los atributos necesarios para el ambiente TEST
    """
    @classmethod
    def env(cls):
        return 'test'

    environment = 'TEST'
    prepaid_account_id = '976626739427'
    core_account_id = '091404209220'
    scoring_process_account_id = '769960225682'
    debit_mex_account_id = '852466501126'
    loans_account_id = '813173849252'
    loans_mex_account_id = '976972312289'
    collections_account_id = '718892725114'
    savings_account_id = '536626836228'
    exchange_account_id = '415373224057'
    adquirencia_account_id = '362383583991'
    bo_mex_account_id = '646535246015'
    debit_col_account_id = '335662396123'
    perfilamiento_account_id = '405941121845'
    core_col_account_id = '165960439435'
    engagement_arg_account_id = '929542502830'
    cxapp_arg_account_id = '143056121290'
    cxapp_mex_account_id = '505341329277'
    cxapp_account_id = '253559369492'
    platform = '847571062705'
    platform_col_account_id = '887067879911'
    platform_mex_account_id = '279186145316'
    platform_mex_bank_account_id = ''
    platform_arg_account_id = '847571062705'
    remittances_mex_account_id = '487035942257'

    region = 'us-east-1'
    
    debit_mex_profile_name = 'uala-mex-debit-test'
    scoring_process_profile_name = 'uala-arg-scoring-process-test'
    bo_mex_profile_name = 'uala-mex-backoffice-test'
    debit_col_profile_name = 'uala-col-debit-test'
    core_col_profile_name = 'uala-col-core-test'
    loans_mex_profile_name = 'uala-mex-loans-test'
    engagement_arg_profile_name = 'uala-arg-engagement-test'
    cxapp_arg_profile_name = 'uala-arg-cxapp-test'
    cxapp_mex_profile_name = 'uala-mex-cxapp-test'
    cxapp_profile_name = 'uala-cxapp-test'
    platform_col_profile_name = 'uala-col-platform-test'
    platform_mex_profile_name = 'uala-mex-platform-test'
    platform_mex_bank_profile_name = ''
    platform_arg_profile_name = 'uala-arg-platform-test'
    remittances_mex_profile_name = 'uala-mex-remittances-test'

    ci_role_names = {
        'prepaid': 'iam_r_uala_arg_prepaid_test_github_actions',
        'core': 'iam_r_uala_arg_core_test_regression',
        'bancar': 'iam_r_uala_arg_prepaid_cross_account_github_prepaid',
        'loans': 'iam_r_uala_arg_loans_test_github_actions',
        'loans_mex': 'iam_r_uala_mex_loans_test_github_actions',
        'collections': 'iam_r_uala_arg_collections_test_github_actions',
        'debit_mex': 'iam_r_uala_mex_debit_test_regression_test',
        'adquirencia': 'iam_r_uala_arg_adquirencia_test_regression',
        'scoring_process': 'iam_r_uala_arg_scoring_process_test_github_actions',
        'bo_mex': 'iam_r_uala_mex_backoffice_test_regression_test',
        'debit_col': 'iam_r_debit_regression',
        'core_col': 'iam_r_core_regression',
        'perfilamiento': 'iam_r_uala_arg_profilementfraud_test_regressions',
        'engagement_arg': 'iam_r_uala_arg_engagement_test_github_actions',
        'cxapp_arg': 'iam_r_uala_arg_cxapp_test_github_actions',
        'cxapp_mex': 'iam_r_uala_mex_cxapp_test_github_actions',
        'cxapp': 'iam_r_uala_global_cxapp_test_github_actions',
        'platform_col': 'iam_r_platform_regression',
        'platform_mex': 'iam_r_platform_regression',
        'platform_mex_bank': 'iam_r_platform_regression',
        'platform_arg': 'iam_r_platform_regression',
        'remittances_mex': 'iam_r_uala_mex_remittances_test_github_actions'
    }


class ProfileStage(Profile):
    """
    Clase que genera un objeto con los atributos necesarios para el ambiente STAGE
    """
    @classmethod
    def env(cls):
        return 'stage'

    environment = 'STAGE'
    prepaid_account_id = '700816522102'
    core_account_id = '452018135714'
    scoring_process_account_id = '956046790186'
    debit_mex_account_id = '172141069965'
    loans_account_id = '333737172075'
    loans_mex_account_id = '341123260173'
    collections_account_id = '557653594106'
    savings_account_id = '477186443130'
    exchange_account_id = '506823503152'
    adquirencia_account_id = '764933410181'
    bo_mex_account_id = '314995385417'
    debit_col_account_id = '158642248137'
    perfilamiento_account_id = '739086639516'
    core_col_account_id = '018204175679'
    engagement_arg_account_id = '453543260168'
    cxapp_arg_account_id = '035123369580'
    cxapp_mex_account_id = '450262403566'
    cxapp_account_id = '048213506444'
    bank_debit_mex_account_id = '208639027208'
    platform = '530983549757'
    platform_col_account_id = '384637447246'
    platform_mex_account_id = '562246686863'
    platform_mex_bank_account_id = '898404650099'
    platform_arg_account_id = '530983549757'
    platform_arg_bank_account_id = '628884961738'
    adquirencia_mex_account_id = '239239011211'
    remittances_mex_account_id = '568011538388'
    adquirencia_col_account_id = '029775111745'
    platform_arg_bank_account_id = '628884961738'
    debit_arg_bank_account_id = '888441683163'
    core_arg_bank_account_id = '261527560251'

    region = 'us-east-1'
    
    debit_mex_profile_name = 'uala-mex-debit-stage'
    scoring_process_profile_name = 'uala-arg-scoring-process-stage'
    bo_mex_profile_name = 'uala-mex-backoffice-stage'
    playground_account_id = '930894468516'
    playground_profile_name = 'uala-arg-playground-prod'
    debit_col_profile_name = 'uala-col-debit-stage'
    core_col_profile_name = 'uala-col-core-stage'
    loans_mex_profile_name = 'uala-mex-loans-stage'
    engagement_arg_profile_name = 'uala-arg-engagement-stage'
    cxapp_arg_profile_name = 'uala-arg-cxapp-stage'
    cxapp_mex_profile_name = 'uala-mex-cxapp-stage'
    cxapp_profile_name = 'uala-cxapp-stage'
    bank_debit_mex_profile_name = 'uala-mex-bank_debit-stage'
    platform_col_profile_name = 'uala-col-platform-stage'
    platform_mex_profile_name = 'uala-mex-platform-stage'
    platform_mex_bank_profile_name = 'uala-mex-bank_platform-stage'
    platform_arg_profile_name = 'uala-arg-platform-stage'
    platform_arg_bank_profile_name = 'uala-arg-bank_platform-stage'
    adquirencia_mex_profile_name = 'uala-mex-adquirencia-stage'
    remittances_mex_profile_name = 'uala-mex-remittances-stage'
    adquirencia_col_profile_name = 'uala-col-adquirencia-stage'
    platform_arg_bank_profile_name = 'uala-arg-bank_platform-stage'
    debit_arg_bank_profile_name = 'uala-arg-bank_debit-stage'
    core_arg_bank_profile_name = 'uala-arg-bank_core-stage'

    ci_role_names = {
        'prepaid': 'iam_r_uala_arg_prepaid_stage_github_actions',
        'core': 'iam_r_uala_arg_core_stage_regression',
        'bancar': 'iam_r_uala_arg_prepaid_cross_account_github_prepaid',
        'loans': 'iam_r_uala_arg_loans_stage_github_actions',
        'loans_mex': 'iam_r_uala_mex_loans_stage_github_actions',
        'collections': 'iam_r_uala_arg_collections_stage_github_actions',
        'debit_mex': 'iam_r_uala_mex_debit_stage_regression_test',
        'adquirencia': 'iam_r_uala_arg_adquirencia_stage_regression',
        'scoring_process': 'iam_r_uala_arg_scoring_process_stage_github_actions',
        'bo_mex': 'iam_r_uala_mex_backoffice_stage_regression_test',
        'debit_col': 'iam_r_debit_regression',
        'core_col': 'iam_r_core_regression',
        'perfilamiento': 'iam_r_uala_arg_profilementfraud_stage_regressions',
        'engagement_arg': 'iam_r_uala_arg_engagement_stage_github_actions',
        'cxapp_arg': 'iam_r_uala_arg_cxapp_stage_github_actions',
        'cxapp_mex': 'iam_r_uala_mex_cxapp_stage_github_actions',
        'cxapp': 'iam_r_uala_global_cxapp_stage_github_actions',
        'bank_debit_mex': 'iam_r_uala_mex_bank_debit_stage_github_actions',
        'platform_col': 'iam_r_platform_regression',
        'platform_mex': 'iam_r_platform_regression',
        'platform_mex_bank': 'iam_r_uala_mex_bank_platform_stage_github_actions',
        'platform_arg': 'iam_r_platform_regression',
        'platform_arg_bank': 'iam_r_platform_regression',
        'adquirencia_mex': 'iam_r_uala_mex_adquirencia_stage_github_actions',
        'remittances_mex': 'iam_r_uala_mex_remittances_stage_github_actions',
        'adquirencia_col': 'iam_r_uala_col_adquirencia_stage_github_actions',
        'platform_arg_bank': 'iam_r_bank_platform_regression',
        'debit_arg_bank': 'iam_r_debit_regression',
        'core_arg_bank': 'iam_r_bank_core_regression'
    }
