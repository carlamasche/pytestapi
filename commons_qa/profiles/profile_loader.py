"""
Archivo py con la clase encargada de cargar los perfiles
"""
import configparser

from commons_qa import logger
import os

from commons_qa.profiles.profile_data import Profile


class ProfileLoader:
    """
    Clase encargada de cargar los perfiles
    """
    _logger = logger.get_logger(__name__)

    @classmethod
    def get_profile(cls, config_file_path=None):
        """
        Método que retorna todos los datos del perfil seleccionado. En el caso de que se requiera agregar más datos al perfil
        es posible hacerlo leyendolos desde un archivo .ini cuyo path debe ser pasado por parámetro.

        :param config_file_path: ruta del archivo para cargar mas datos a un perfil
        :return: objeto con todos los datos de un perfil
        """
        current_env = os.getenv('env')
        profile = Profile.for_env(current_env)

        if config_file_path is not None:
            parser = configparser.ConfigParser()
            parser.read(config_file_path)

            config_section_values = parser[current_env]

            for key in config_section_values:
                _property = config_section_values.get(key)
                setattr(profile, key, _property)

        return profile
