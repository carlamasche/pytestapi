"""
Archivo py con la clase encargada de conectarse con el servicio DynamoDB de AWS
"""
from commons_qa import logger

from commons_qa.configs.authenticator import Authenticator


class DynamoInvoker:
    """
    Esta clase contiene todos los métodos necesarios para conectarse con el servicio DynamoDB de AWS
    """
    AWS_SERVICE_NAME = 'dynamodb'

    def __init__(self, account):
        self._client = Authenticator.for_account_and_service(account, self.AWS_SERVICE_NAME).client
        self.logger = logger.get_logger(__name__)

    def get_list_by_query(self, table_name, key_condition_expression, expression_attributes_values):
        """
        Este método permite traer un listado de registros de una tabla a partir de una query

        :param table_name: Nombre de la tabla de donde traeremos el listado
        :param key_condition_expression: Expresión de condición a cumplir para agregar el registro al listado
        :param expression_attributes_values: Valor o valores utilizados para reemplazar en las expresiones
        :return type: dict
        :return: Listado de resultados de registros

        Ejemplo::

            get_list_by_query(table_name= "onboarding_users",
                              key_condition_expression= "userId = :us",
                              expression_attributes_values={':us': {"S": 'cd9f5bbb-4282-401a-9677-cd1456384a27'}})
        """
        self.logger.info("Querying items in {} table...".format(table_name))
        result = self._client.query(
            TableName=table_name,
            KeyConditionExpression=key_condition_expression,
            ExpressionAttributeValues=expression_attributes_values
        )
        return result

    def get_list_by_query_with_index(self, table_name, index_name, key_condition_expression,
                                     expression_attributes_values, expression_attributes_names=None, filter_expression=None):
        """
        Este método permite traer un listado de registros de una tabla con índice a partir de una query

        :param table_name: Nombre de la tabla de donde traeremos el listado
        :param index_name: Nombre del index de la tabla
        :param key_condition_expression: Expresión de condición a cumplir para agregar el registro al listado
        :param expression_attributes_values: Valor o valores utilizados para reemplazar en las expresiones
        :param expression_attributes_names: (Opcional - Valor por defecto None) Cuando un atributo entra en conflicto con una palabra reservada de DynamoDB, o contiene caracteres especiales que podrían
            ser no interpretados en una expression, se debe anteponer el caracter # en la expresión para diferenciar el nombre del atributo. Por ejemplo, dado el siguiente nombre de
            atributo::

                    Percentile

            Este nombre entraría en conflicto con una palabra reservada, por lo que no puede ser usada directamente en una expresion, en este caso se usará::

                    {"#P":"Percentile"}

            Y luego se sustituirá en la expresión de la siguiente manera::

                        #P = :val

        :param filter_expression: (Opcional - Valor por defecto None) Condición de filtrado de resultados
        :return type: dict
        :return: Listado de resultados de registros

        Ejemplo::

            get_list_by_query(table_name= "onboarding_users",
                              index_name='Status-index',
                              key_condition_expression= "userId = :userId",
                              expression_attributes_values={':userId': {"S": 'cd9f5bbb-4282-401a-9677-cd1456384a27'}}
                              )
        """
        self.logger.info("Querying items using index in {} table...".format(table_name))
        dynamo_params = {
            "TableName": table_name,
            "IndexName": index_name,
            "KeyConditionExpression": key_condition_expression,
            "ExpressionAttributeValues": expression_attributes_values
        }
        if expression_attributes_names is not None:
            dynamo_params["ExpressionAttributeNames"] = expression_attributes_names
        if filter_expression is not None:
            dynamo_params["FilterExpression"] = filter_expression
        result = self._client.query(
            **dynamo_params
        )
        return result

    def get(self, table_name, key, consistent_read=True):
        """
        Este método permite traer un registro de la tabla

        :param table_name: Nombre de la tabla dónde se va a buscar el registro
        :param key: Key o Keys que identifican al registro
        :param consistent_read: (Opcional - Valor por defecto True) Determina la consistencia del modelo de lectura. Si es True, la operación usa lectura fuertemente consistente;
            caso contrario, la operación usa lectura eventualmente consistente.
        :return type: dict

        Ejemplo::

            get(table_name='transactions', key={'AccountFrom': {'S': 'cd9f5bbb-4282-401a-9677-cd1456384a27'}}
        """
        self.logger.info("Getting item in {} table...".format(table_name))
        result = self._client.get_item(
            TableName=table_name,
            Key=key,
            ConsistentRead=consistent_read
        )
        return result

    def delete(self, table_name, keys):
        """
        Este método permite eliminar un registro de la tabla

        :param table_name: Nombre de la tabla de dónde será eliminado el registro
        :param keys: Key o Keys con las que se identifica al registro

        Ejemplo::

            delete(table_name='transactions',
                   keys={'AccountFrom': {'S': 'cd9f5bbb-4282-401a-9677-cd1456384a27'},
                         'TransactionId': {'S': '05e9453a7f9a49b8ab0fc3f89f99fc8e-112633'}
                         })
        """
        self.logger.info("Deleting item in {} table".format(table_name))
        self._client.delete_item(TableName=table_name, Key=keys)

    def update(self, table_name, key_condition_expression, update_expression, expression_attribute_values=None,
               expression_attribute_names=None):
        """
        Este método permite actualizar un registro

        :param table_name: Nombre de la tabla dónde actualizaremos el registro
        :param key_condition_expression: Expresión de condición a cumplir para encontrar el registro a actualizar
        :param update_expression: Expresión para la actualización del registro
        :param expression_attribute_values: (Opcional) Valor o valores utilizados para reemplazar en las expresiones
        :param expression_attribute_names: (Opcional - Valor por defecto None) Cuando un atributo entra en conflicto con una palabra reservada de DynamoDB, o contiene caracteres
            especiales que podrían ser no interpretados en una expression, se debe anteponer el caracter # en la expresión para diferenciar el nombre del atributo.
            Por ejemplo, dado el siguiente nombre de atributo::

                    Percentile

            Este nombre entraría en conflicto con una palabra reservada, por lo que no puede ser usada directamente en una expresion, en este caso se usará::

                    {"#P":"Percentile"}

            Y luego se sustituirá en la expresión de la siguiente manera::

                        #P = :val
        :return type: dict
        :return: Registro actualizado

        Ejemplo::

            update(table_name= "onboarding_users",
                   key_condition_expression= "userId = :userId",
                   update_expression='SET #status = :user_status',
                   expression_attributes_values={':userId': {"S": 'cd9f5bbb-4282-401a-9677-cd1456384a27'},
                                                 ':user_status' : {"S" : 'PENDING_VALIDATION'}
                   expression_attribute_names={"#status": "Status"})

        """
        dynamo_params = {
            "TableName": table_name,
            "Key": key_condition_expression,
            "UpdateExpression": update_expression,
            "ReturnValues": "UPDATED_NEW"
        }
        if expression_attribute_names is not None:
            dynamo_params["ExpressionAttributeNames"] = expression_attribute_names

        if expression_attribute_values is not None:
            dynamo_params["ExpressionAttributeValues"] = expression_attribute_values

        self.logger.info("Updating item in {} table".format(table_name))
        result = self._client.update_item(
            **dynamo_params
        )
        return result

    def delete_item(self, table_name, key_condition_expression, update_expression, expression_attribute_names=None):
        """
        Este método permite actualizar un registro, eliminando un item (campo) del mismo

        :param table_name: Nombre de la tabla dónde actualizaremos el registro
        :param key_condition_expression: Expresión de condición a cumplir para encontrar el registro a actualizar
        :param update_expression: Expresión para la actualización del registro
        :param expression_attribute_names: (Opcional - Valor por defecto None) Cuando un atributo entra en conflicto con una palabra reservada de DynamoDB, o contiene caracteres
            especiales que podrían ser no interpretados en una expression, se debe anteponer el caracter # en la expresión para diferenciar el nombre del atributo.
            Por ejemplo, dado el siguiente nombre de atributo::

                    Percentile

            Este nombre entraría en conflicto con una palabra reservada, por lo que no puede ser usada directamente en una expresion, en este caso se usará::

                    {"#P":"Percentile"}

            Y luego se sustituirá en la expresión de la siguiente manera::

                        #P = :val
        :return type: dict

        Ejemplo::

            delete_item(table_name= "accounts",
                        key_condition_expression={'AccountId': {'S': '8d901814-6d96-424a-a10e-f28012ab5f1c'}},
                        update_expression='REMOVE #at',
                        expression_attribute_names={'#at': 'Pin'})
        """
        dynamo_params = {
            "TableName": table_name,
            "Key": key_condition_expression,
            "UpdateExpression": update_expression,
            "ReturnValues": "UPDATED_NEW"
        }

        if expression_attribute_names is not None:
            dynamo_params["ExpressionAttributeNames"] = expression_attribute_names

        self.logger.info("Updating item in {} table".format(table_name))
        result = self._client.update_item(
            **dynamo_params
        )
        return result

    def create(self, table_name, item):
        """
        Este método permite crear un registro en la tabla

        :param table_name: Nombre de la tabla dónde crearemos el registro
        :param item: Registro a crear en la tabla

        Ejemplo::

            create(table_name='transactions',
                   item='{"AccountFrom": {"S": "05e9453a-7f9a-49b8-ab0f-c3f89f99fc8e"},
                          "Amount": {"N": "17"},
                          "AuthorizationId": {"S": "20201026092850-05e9453a-6631668"}}')
        """
        self._client.put_item(TableName=table_name, Item=item)

    def list_tables(self):
        """
        Este método devuelve el listado de tablas de Dynamo

        :return type: dict
        :return: Listado de tablas en Dynamo

            Sintáxis de la respuesta::
                {
                    'TableNames': ['string',],
                    'LastEvaluatedTableName': 'string'
                }
        """
        return self._client.list_tables()

    def scan(self, table_name):
        """
        Este método retorna registros de la tabla pasada por parámetro (con límite de 1 MB)

        :param table_name: Nombre de la tabla
        :return type: dict
        :return: Listado de registros de la tabla pasada por parámetro
        """
        return self._client.scan(TableName=table_name)

    def count_items(self, table_name):
        """
        Este método retorna la cantidad de registros de la tabla pasada por parámetro (con límite de 1 MB)

        :param table_name: Nombre de la tabla
        :return type: int
        :return: Cantidad de registros de la tabla pasada por parámetro
        """
        scan_response = self.scan(table_name)
        count_registers = scan_response['Count']
        return count_registers
