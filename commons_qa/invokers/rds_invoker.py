"""
Archivo py con la clase encargada de conectarse con el servidor de base de datos PostgreSQL
"""
import logging

import psycopg2


class RDSInvoker:

    def __init__(self, profile):
        self.logger = logging.getLogger(__name__)
        self.profile = profile

    def get_connection(self):
        """ Connect to the PostgreSQL database server """
        try:
            self.logger.info('Connecting to the PostgreSQL database...')
            return psycopg2.connect(host=self.profile.db_host,
                                    database=self.profile.db_database,
                                    user=self.profile.db_user,
                                    password=self.profile.db_pass)

        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.info(error)

    def close_connection(self, connection):
        if connection is not None:
            connection.close()
            self.logger.info('Database connection closed.')

    def execute_query(self, query, params=(), is_delete = False):
        """
        Este método permite hacer consultas en las tablas de RDS.
        :param query: Se indica la consulta que se quiere ejecutar.
        :param params: Parametros de la query que se ejecuta

        Ejemplo::

            * SELECT idcoelsa, status, type, motivo, oricvucbu, desticbu FROM platform.cvutransfers where idcoelsa='id'
        """
        self.logger.info("Executing query: {} on RDS".format(query))
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute(query, params)
        if is_delete:
            conn.commit()
            self.logger.info("Record/s Deleted successfully")
            self.close_connection(conn)
        else:
            result = cursor.fetchall()
            self.close_connection(conn)
            return result

    def commit_query(self, query):
        """
        Este método permite hacer modificaciones en las tablas de RDS, la acción dependerá de la query utilizada
        :param query: Se indica la acción que se quiere ejecutar, sobre que tabla y con que condiciones.

        Ejemplo::

            * DELETE FROM platform.cvutransfers WHERE idcoelsa='valor_id'
            * INSERT INTO platform.cvutransfers (idcoelsa, status) values('valor_idcoelsa','valor_status')
        """
        self.logger.info("Commit query: {} on RDS".format(query))
        conn = self.get_connection()
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        self.close_connection(conn)
